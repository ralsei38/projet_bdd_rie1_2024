# deploiement

Il vaut mieux lancer l'API le front et docker dans 3 shells différents pour pouvoir suivre les logs pendant le développement.

Dans un premier temps il faut installer / lancer l'API
```bash
git clone git@gitlab.com:ralsei38/projet_bdd_rie1_2024.git
cd projet_bdd_rie1_2024
java ????
```

il faut ensuite lancer la base de données de développement
```bash
cd projet_bdd_rie1_2024
docker-compose up --build
```

Pour finir, il faut également lancer le serveur web de developpement
```bash
cd projet_bdd_rie1_2024/front
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
flask run
```

la variable `API_ENDPOINT` dans le fichier `front/app.py` doit être correspondre à votre IP !
à terme, il faudra mettre cette variable dans un fichier .env
# Use the official MySQL image from the Docker Hub
FROM mysql:8.0

# Set environment variables for the root password and database name

ENV MYSQL_ROOT_PASSWORD=root
ENV MYSQL_DATABASE=database

# Copy the SQL scripts into the Docker image
COPY database/create.sql /docker-entrypoint-initdb.d/
COPY database/insert.sql /docker-entrypoint-initdb.d/


# The official MySQL image automatically runs any scripts
# found in /docker-entrypoint-initdb.d/ during the container's startup.

# build
# docker build -t my-mysql-image .

# runs
# docker run --name my-mysql-container -d my-mysql-image

# pas besoin de faire des volumes
# on écrit un insert.sql qui est lancé par défaut lors du démarrage du conteneur.

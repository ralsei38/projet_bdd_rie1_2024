# README
## Conception

### Propriété élémentaire

Produit: _id_, categorie, nom, stock, prix de revient

Salle de ventes:_id_, categorie, type

Type: _nom_

catégorie: _nom_, description

Vente: _id_, prix, révocable, surenchère_possible, duree, active

Enchère: _id_, prix achat, time, quantite_produit  

Utilisateurs: _email_, nom, prénom, adresse, password

### Dépendance fonctionnel 

Produit => catégorie
Salle de ventes => categorie, type
Vente => produit, salle de vente, utilisateur, type
Enchère => vente, utilisateur

### Contrainte de valeur

le délai maximal entre deux offres est fixé à 10 minutes
```
délai < 10
```

Le stock ne peut pas etre inferieur a 0
```
stock >= 0

```

Le prix de reviens doit obligatoirement etre supérieur à 0
```
prix_de_reviens_produit > 0
```

Le prix de reviens doit obligatoirement etre supérieur à 0
```
prix_de_depart_vente > 0
```

- nouveaux_prix_proposé_montant > prix_actuel_montant
- nouveaux_prix_proposé_descendant > prix_actuel_descendant

un utilisateur (owner) d'une vente ne peut pas encherir sur celle-ci
```
utilisateur qui encherit != utilisateur_owner
```

La quantité lors d'une enchère doit être > 0 
```
quantite_produit > 0 
```

une quantité précisé lors d'un enchere ne peut pas excéder la quantité de produit de la vente concernée
```
enchere.quantite <= vente.produit.quantite
```

### Contrainte de multiplicité

- Un salle des ventes contients plusieurs ventes
- une vente = un produit
- une ventes peut avoir plusieurs enchères
- une enchère ne peut concerné qu'une seule vente
- Selon le type de vente, une enchère par utilisateur


### Autres contraintes

- Une enchère toute les 10 min par utilisateur
- Si enchère descendante, la première enchère remporte la vente
- Un utilisateur ne peut pas mettre en vente et faire une echère sur celle-ci (vue ou backend ?)

## dépendances / recherches sur l'API
- https://sparkjava.com/ + pom.xml depency management
- OPENJDK version 21.0.3 sur windows
- intelliJ IDEA 2024.1.2
- https://insomnia.rest/download
- https://mvnrepository.com/artifact/com.oracle.database.jdbc/ojdbc11
- https://dev.mysql.com/doc/connector-j/en/connector-j-usagenotes-connect-drivermanager.html
- https://dev.mysql.com/doc/connector-j/en/connector-j-installing.html
- https://mvnrepository.com/artifact/org.json/json/20240303
- création d'un `Dockerfile` Mysql qui push `create.sql` et `insert.sql` at startup
- docker build -t my-mysql-image .
- docker run --name my-mysql-container -e MYSQL_ROOT_PASSWORD=root -e MYSQL_DATABASE=database -p 3306:3306  my-mysql-image


## API

![](./API_screenshot.PNG)
### Users Endpoint

- [x] **GET /users**: Retrieve all users.
- [x] **GET /users/{email}**: Retrieve a specific user by email.
- [x] **POST /users**: Create a new user.
- [x] **PUT /users/{email}**: Update an existing user.
- [x] **DELETE /users/{email}**: Delete a user by email.

### Sales Endpoint

- [x] **GET /sales**: Retrieve all sales.
- [x] **GET /sales/{id}**: Retrieve a specific sale by ID.
- [x] **POST /sales**: Create a new sale.
- [x] **PUT /sales/{id}**: Update an existing sale.
- [x] **DELETE /sales/{id}**: Delete a sale by ID.

### Bids Endpoint

- [x] **GET /bids**: Retrieve all bids.
- [x] **GET /bids/{id}**: Retrieve a specific bid by ID.
- [x] **POST /bids**: Create a new bid.
- [x] **PUT /bids/{id}**: Update an existing bid.
- [x] **DELETE /bids/{id}**: Delete a bid by ID.

### TODO
** il faut penser a bien set des codes de retour pour faciliter le travail coté flask**

- [ ] fix les entrées, si null, doit être bloqué coté JS ou backend
- [ ] un code d'erreur + message de retour lorsque le bid ne peut pas etre créer
- [ ] vérifier coté API que le format des parametres est bon (pas string a la place d'entiers par exemple)
- [ ] ajouter une AUTO_INCREMENT NOT NULL PRIMARY KEY dans le schéma de "ventes"
- [ ] faire un choix sur les noms de table: soit enchere et vente / soit bid et sales, pas un mix des deux
- [ ] Pour le moment un put sur /demande et les autres demande de renseigner tout les champs ! une solution ?
- [ ] Lors d'un appel sur /sales en put, il n'est pas possible de changer le mail de la personne. c'est une foreign key ça demanderait d'éditer a plusieurs endroits
étant donné qu'un mail n'est pas modifiable, une personne ne devrait pas pouvoir changer le mail lié a une enchere, donc pas fait pour le moment.
- [ ] PUT dans le vide devrait au moins retourner un not found 404 et un message avec

## web GUI

installation / lancement de l'application web
```bash
# installation des dépendendances
sudo apt install -y python3-venv python3-pip
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```

il faudra activier l'environnement virtuel avant de travailler !
de cette manière les imports python fonctionneront correctement.
```bash
source venv/bin/activate
```
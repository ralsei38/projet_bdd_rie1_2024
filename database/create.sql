CREATE TABLE IF NOT EXISTS Users(
    email VARCHAR(255) PRIMARY KEY,
    firstname VARCHAR(255),
    lastname VARCHAR(255),
    address VARCHAR(255),
    password VARCHAR(255)
    );

CREATE TABLE IF NOT EXISTS Sales (
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    product_name VARCHAR(255),
    price FLOAT CHECK(price > 0),
    revocable BOOLEAN NOT NULL DEFAULT 0,
    overbid BOOLEAN NOT NULL DEFAULT 1,
    duration TIMESTAMP,
    type VARCHAR(255) DEFAULT 'Amount',
    email VARCHAR(255) NOT NULL,
    active BOOLEAN NOT NULL DEFAULT 1,
    last_bid_time TIMESTAMP,
    FOREIGN KEY (email) REFERENCES Users(email) ON DELETE CASCADE
    
    );

CREATE TABLE IF NOT EXISTS Bids (
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    purchase_price float CHECK(purchase_price > 0),
    time TIMESTAMP NOT NULL,
    quantity int NOT NULL,
    email VARCHAR(255) NOT NULL,
    sale_id INT,
    FOREIGN KEY (sale_id) REFERENCES Sales(id) ON DELETE CASCADE,
    FOREIGN KEY (email) REFERENCES Users(email) ON DELETE CASCADE
    );

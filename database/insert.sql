INSERT INTO Users VALUES("elian.loraux@gmail.com", "Elian",  "Loraux", "38 chemin des bruyeres, Tignieu", "password");
INSERT INTO Users VALUES("arthur.gaillard@gmail.com", "Arthur", "Gaillard", "14 boulevard general Gallieni, Grenoble", "password");
INSERT INTO Users VALUES("theo.fontana@gmail.com", "Theo", "Fontana", "681 Rue de la Passerelle, Saint-Martin-d'Heres", "password");

INSERT INTO Sales VALUES ('1', 'Ferrarie f40', '10', '0', '1', NULL, 'Amount', 'elian.loraux@gmail.com','1', now());
INSERT INTO Sales VALUES ('2', 'NVIDIA RTX 4090','100', '0', '1', NULL, 'Amount', 'elian.loraux@gmail.com','1', now());
INSERT INTO Sales VALUES ('3', 'Le petit vin blanc du departement', '700', '0', '1', NULL, 'Amount', 'arthur.gaillard@gmail.com','1', now());

INSERT INTO Bids VALUES ('1', '110', now(), '1', 'arthur.gaillard@gmail.com', '2');
INSERT INTO Bids VALUES ('2', '80', now(), '1', 'theo.fontana@gmail.com', '1');
INSERT INTO Bids VALUES ('3', '860', now(), '1', 'elian.loraux@gmail.com', '3');
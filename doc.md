# Projet baie éléctronique - Arthur Gaillard, Théo Fontana et Elian Loraux

## Conception

### Base de donnée

#### Propriété élémentaire

Produit: _id_, categorie, nom, stock, prix de revient

Salle de ventes:_id_, categorie, type

Type: _nom_

catégorie: _nom_, description

Vente: _id_, Produit, salle des vente, type, utilisateur, prix de départ, révocable, surenchère_possible, duree, prix_actuel

Enchère: _id_, Vente, utilisateur, prix achat, time, quantite_produit  

Utilisateurs: _email_, nom, prénom, adresse postale

#### Contrainte de valeur

le délai maximal entre deux offres est fixé à 10 minutes
```
délai < 10
```

Le stock ne peut pas etre inferieur a 0
```
stock >= 0

```

Le prix de reviens doit obligatoirement etre supérieur à 0
```
prix_de_reviens_produit > 0
```

Le prix de reviens doit obligatoirement etre supérieur à 0
```
prix_de_depart_vente > 0
```

- nouveaux_prix_proposé_montant > prix_actuel_montant
- nouveaux_prix_proposé_descendant > prix_actuel_descendant

un utilisateur (owner) d'une vente ne peut pas encherir sur celle-ci
```
utilisateur qui encherit != utilisateur_owner
```

La quantité lors d'une enchère doit être > 0
```
quantite_produit > 0
```

une quantité précisé lors d'un enchere ne peut pas excéder la quantité de produit de la vente concernée
```
enchere.quantite <= vente.produit.quantite
```

#### Contrainte de multiplicité

- Un salle des ventes contients plusieurs ventes
- une vente = un produit
- une ventes peut avoir plusieurs enchères
- une enchère ne peut concerné qu'une seule vente
- Selon le type de vente, une enchère par utilisateur


#### Autres contraintes

- Une enchère toute les 10 min par utilisateur
- Si enchère descendante, la première enchère remporte la vente
- Un utilisateur ne peut pas mettre en vente et faire une echère sur celle-ci (vue ou backend ?)

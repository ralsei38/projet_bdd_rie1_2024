from datetime import datetime
from apscheduler.schedulers.background import BackgroundScheduler
from flask import *
import requests
import json

app = Flask(__name__)
app.secret_key ="m(2kWb6]8K2R.b"
API_ENDPOINT="192.168.199.106:4567"


def sensor():
    sales = get_active_sales()
    for sale in sales:
        if "last_bid_time" in sale:
            min = compare_time(sale['last_bid_time'])
            if min >= 10:
                requests.put(f"http://{API_ENDPOINT}/sales/"+str(sale['id'])+"/archived")
                print("Sale n°"+str(sale['id'])+ " archived")


sched = BackgroundScheduler(daemon=True)
sched.add_job(sensor,'interval',seconds=10)
sched.start()


##### INDEX ######
@app.route("/", methods=['GET', 'POST'])
def index():
    """route to the home page"""
    sales = get_active_sales()
    if "email" in session:
         flash(f"Hello {session['firstname']} {session['lastname']}")
    return render_template('index.html', sales=sales)


##### LOGIN ######
@app.route("/login", methods=['GET', 'POST'])
def login():
    """Logs in user via API call"""
    error = None
    if request.method == "POST":
        email = request.form.get("email", None)
        password = request.form.get("password", None)
        if valid_login(email, password):
                flash("You've succefully log in !")
                return redirect(url_for('index'))
        error = 'Invalid username/password'
    return render_template('login.html', error=error)

##### REGISTER ######
@app.route("/register", methods=['GET', 'POST'])
def register():
    """This route registers a new user via API call"""
    error = None
    if request.method == "POST":
        # register_request = requests.post(url,request.form)
        if valid_register(request.form):
            flash("Your account has been created successfully !")
            session.pop("email", None)
            session.pop("firstname", None)
            session.pop("lastname", None)
            session.pop("address", None)
            return redirect(url_for('login'))
        else:
            # already exist OR wrong parameters in form
            error = 'An error occured...'
    return render_template('register.html', error=error)


##### LOGOUT ######
@app.route("/logout")
def logout():
    """Logs out user via API call"""
    session.pop("email", None)
    session.pop("firstname", None)
    session.pop("lastname", None)
    session.pop("address", None)
    flash("You've succefully log out !")
    return redirect(url_for("index"))


##### SALES #####
@app.route("/sales/<id>", methods=['GET', 'POST'])
def sales(id):
    if "email" in session :
        if valid_sales_id(id):
            sale = get_sales_by_id(id)
            if sale['active'] == 0:
                flash("This sale is closed !")
                return redirect(url_for("index"))
            if request.method == "POST":
                data = dict(request.form)
                data['email'] = session['email']
                data['sale_id'] = id
                if update_sale_price(id, data['purchase_price']): # On test d'abords la maj du prix de la vente
                    response = requests.post(f"http://{API_ENDPOINT}/bids",data)
                    if response.status_code == 201: # Ensuite on vient tester la création de l'enchère
                        flash("The bids has been created")
                        return redirect("http://localhost:5000/sales/"+id)
                    else:
                        flash("ERROR, the bids could not be created...")
                        return redirect("http://localhost:5000/sales/"+id)
                else:
                        flash("ERROR updating the sale...")
                        return redirect("http://localhost:5000/sales/"+id)
            min = None
            bids = get_bids_by_sale(id)
            if "last_bid_time" in sale:
                min = compare_time(sale['last_bid_time'])
            return render_template("sales.html", sale=sale, bids=bids, min=min)
        else:
            return redirect(url_for("index"))
    else:
        flash("You must be logged in to access this page.")
        return redirect(url_for("login"))


##### CREATE SALE #####
@app.route("/sales/new", methods=['GET', 'POST'])
def create_sale():
    if "email" not in session :
        flash("You must be logged in to create a sale.")
        return redirect(url_for("login"))
    if request.method == "POST":
        #API call to create the sale
        data = dict(request.form)
        data['email'] = session['email']
        print(data)
        #if data['duration'] == '': 
        # Pour le moment j'ai mis la gestion de la duration en pause car on a un problème de parsing
        # Donc j'ai juste fait en sorte que par défaut la duration = illimited
        response = requests.post(f"http://{API_ENDPOINT}/sales/illimited",data) 
        # else:
        #     print(data)
        #     response = requests.post(f"http://{API_ENDPOINT}/sales/limited",data)

        if response.status_code == 200:
            flash("The sale has been created")
            return redirect(url_for("userpage"))
        else:
            flash("ERROR, the sale could not be created...")
            return redirect(url_for("userpage"))
    return render_template("create_sale.html")


##### USERPAGE #####
@app.route("/userpage", methods=['GET', 'POST'])
def userpage():
    """
        Sends back the user page in which their sales and auctions are displayed.
    """
    if "email" in session :
        sales = get_sales_by_user(session['email'])
        auctions = get_auctions_by_user(session['email'])
        return render_template("userpage.html", sales=sales, auctions=auctions)
    else:
        flash("You must be logged in to access this page.")
        return redirect(url_for("login"))




########## FUNCTION ##########

def valid_login(email, password):
    """
        Checks if a email matches a password via API call
    """
    if email is None or password is None:
        return False

    data = {
    "email" : email,
    "password": password
    }

    result = requests.post(f"http://{API_ENDPOINT}/login", data)
    if result.status_code == 200:
        #crendentials ok, fetching user information
        user_information_response = requests.get(f"http://{API_ENDPOINT}/users/{email}").content.decode()
        user_information = json.loads(user_information_response)
        # storing information in Flask's session
        for key, value in user_information.items():
            session[key] = value
    return (session.get('email', False))


def valid_register(user_information):
    result = requests.post(f"http://{API_ENDPOINT}/users", user_information)
    return result.status_code

def valid_sales_id(id):
    valid = False
    result = requests.get(f"http://{API_ENDPOINT}/sales")
    for array in result.json():
        if id == str(array['id']):
            valid = True
    return valid

def get_sales_by_user(email):
    response = requests.get(f"http://{API_ENDPOINT}/users/{email}/sales")
    data = response.content.decode()
    sales = {}
    if response.status_code == 200:
        sales = json.loads(data)
    else:
        print("API request malformed ?")
    return sales

def get_auctions_by_user(email):
    response = requests.get(f"http://{API_ENDPOINT}/users/{email}/bids")
    data = response.content.decode()
    auctions = {}
    if response.status_code == 200:
        auctions = json.loads(data)
    else:
        print("API request malformed ?")
    return auctions

def get_user():
    result = requests.get(f"http://{API_ENDPOINT}/users")
    return result.json()

def get_user_by_email(email):
    request = f"http://{API_ENDPOINT}/users/"+email
    result = requests.get(request)
    return result.json()

def get_sales():
    result = requests.get(f"http://{API_ENDPOINT}/sales")
    return result.json()

def get_active_sales():
    result = requests.get(f"http://{API_ENDPOINT}/sales/active")
    return result.json()

def get_sales_by_id(id):
    result = requests.get(f"http://{API_ENDPOINT}/sales/"+id)
    return result.json()

def get_bids_by_sale(id):
    result = requests.get(f"http://{API_ENDPOINT}/sales/"+id+"/bids")
    return result.json()

def update_sale_price(id, price):
    data = {"price": price}
    result = requests.put(f"http://{API_ENDPOINT}/sales/"+id, data)
    if result.status_code == 201:
        return True
    else:
        return False
    
def compare_time(last_bid_time):
    format_date = '%Y-%m-%d %H:%M:%S'
    format_date_with_ms = '%Y-%m-%d %H:%M:%S.%f'
    now = datetime.now()
    current_time = now.strftime('%Y-%m-%d %H:%M:%S')
    date1 = datetime.strptime(current_time, format_date)
    date2 = datetime.strptime(last_bid_time, format_date_with_ms)
    diff = date1 - date2
    min = diff.total_seconds() / 60
    return min


package org.example;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

import static spark.Spark.*;
// Notice, do not import com.mysql.cj.jdbc.*
// or you will have problems!

public class Main {
    public static void main(String[] args) {
        // Establish database connection
        try {
            // mysql is a docker container with root / root login
            String url = "jdbc:mysql://localhost:3306/database?user=root&password=root";
            Connection conn = DriverManager.getConnection(url);



            // GET on /users to fetch all users
            get("/users", (req, res) ->
            {
                try {
                    // Execute SQL query to retrieve users
                    Statement stmt = conn.createStatement();
                    ResultSet rs = stmt.executeQuery("SELECT * FROM Users");

                    JSONArray jsonArray = new JSONArray();

                    while (rs.next()) {
                        JSONObject userJson = new JSONObject();
                        userJson.put("email", rs.getString("email"));
                        userJson.put("firstname", rs.getString("firstname"));
                        userJson.put("lastname", rs.getString("lastname"));
                        userJson.put("address", rs.getString("address"));
                        jsonArray.put(userJson);
                    }
                    res.type("application/json");
                    return jsonArray;

                } catch (SQLException e) {
                    e.printStackTrace();
                    return "An error occured when retrieving users";
                }
            });

            // GET on /users/:email to fetch a specific user
            get("/users/:email", (req, res) ->
            {
                try {
                    // Execute SQL query to retrieve users
                    Statement stmt = conn.createStatement();
                    PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM Users WHERE email = ?");
                    pstmt.setString(1, req.params(":email"));
                    ResultSet rs = pstmt.executeQuery();
                    JSONObject userJson = new JSONObject();
                    if (rs.next())
                    {
                        userJson.put("firstname", rs.getString("firstname"));
                        userJson.put("email", rs.getString("email"));
                        userJson.put("lastname", rs.getString("lastname"));
                        userJson.put("address", rs.getString("address"));
                        res.type("application/json");
                    }
                    return userJson;

                } catch (SQLException e) {
                    e.printStackTrace();
                    return "An error occured when retrieving this user\n exiting...";
                }
            });


            // GET on /users/:email to fetch a specific user
            get("/users/:email/sales", (req, res) ->
            {
                try {
                    // Execute SQL query to retrieve users
                    Statement stmt = conn.createStatement();
                    PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM Sales WHERE email = ?");
                    pstmt.setString(1, req.params(":email"));
                    ResultSet rs = pstmt.executeQuery();
                    List<JSONObject> sales = new ArrayList<>();
                    while(rs.next())
                    {
                        JSONObject userJson = new JSONObject();
                        userJson.put("id", rs.getFloat("id"));
                        userJson.put("product_name", rs.getString("product_name"));
                        userJson.put("price", rs.getFloat("price"));
                        userJson.put("revocable", rs.getBoolean("revocable"));
                        userJson.put("overbid", rs.getBoolean("overbid"));
                        userJson.put("duration", rs.getTimestamp("duration"));
                        userJson.put("type", rs.getString("type"));
                        sales.add(userJson);
                    }
                    res.type("application/json");
                    return sales;


                } catch (SQLException e) {
                    e.printStackTrace();
                    return "An error occured when retrieving the sales of this user\n exiting...";
                }
            });


            // GET on /users/:email to fetch a specific user
            get("/users/:email/bids", (req, res) ->
            {
                try {
                    // Execute SQL query to retrieve users
                    Statement stmt = conn.createStatement();
                    PreparedStatement pstmt = conn.prepareStatement("SELECT Bids.id, Bids.purchase_price, Bids.time, Bids.sale_id, Bids.email, Sales.product_name, Sales.email FROM Bids, Sales WHERE Bids.email = ? AND Bids.sale_id = Sales.id");
                    pstmt.setString(1, req.params(":email"));
                    ResultSet rs = pstmt.executeQuery();
                    List<JSONObject> bids = new ArrayList<>();
                    while(rs.next())
                    {
                        JSONObject userJson = new JSONObject();
                        userJson.put("bids_id", rs.getInt("Bids.id"));
                        userJson.put("purchase_price", rs.getFloat("Bids.purchase_price"));
                        userJson.put("time", rs.getTimestamp("Bids.time"));
                        userJson.put("sale_id", rs.getInt("Bids.sale_id"));
                        userJson.put("vendor", rs.getString("sales.email"));
                        userJson.put("product_name", rs.getString("sales.product_name"));
                        bids.add(userJson);
                    }
                    res.type("application/json");
                    return bids;


                } catch (SQLException e) {
                    e.printStackTrace();
                    return "An error occured when retrieving the sales of this user\n exiting...";
                }
            });

            // POST on /users to create a new user
            post("/users", (req, res) -> {
                try {
                    PreparedStatement pstmt = conn.prepareStatement("INSERT INTO Users (email, firstname, lastname, address, password) VALUES (?, ?, ?, ?, ?)");
                    pstmt.setString(1, req.queryParams("email"));
                    pstmt.setString(2, req.queryParams("firstname"));
                    pstmt.setString(3, req.queryParams("lastname"));
                    pstmt.setString(4, req.queryParams("address"));
                    pstmt.setString(5, req.queryParams("password"));
                    pstmt.executeUpdate();

                    // Return success message
                    res.status(201); // HTTP status code 201 indicates successful creation
                    return "User created successfully";
                } catch (SQLException e) {
                    e.printStackTrace();
                    return "An error occurred when creating the user make sur the API call is correctly written and nothing is missing in the POST data.";
                }
            });




            // PUT on /users to update a user
            put("/users", (req, res) -> {
                try {
                    PreparedStatement pstmt = conn.prepareStatement("UPDATE Users SET lastname = ?, firstname = ?, address = ? WHERE email = ?");
                    pstmt.setString(1, req.queryParams("firstname"));
                    pstmt.setString(2, req.queryParams("lastname"));
                    pstmt.setString(3, req.queryParams("address"));
                    pstmt.setString(4, req.queryParams("email"));
                    pstmt.executeUpdate();

                    // Return success message
                    res.status(201); // HTTP status code 201 indicates successful creation
                    return "User information updated successfully";
                } catch (SQLException e) {
                    e.printStackTrace();
                    return "An error occurred when updating user information";
                }
            });


            // DELETE on /users to delete a user
            delete("/users", (req, res) -> {
                try {
                    PreparedStatement pstmt = conn.prepareStatement("DELETE FROM Users WHERE email = ?");
                    pstmt.setString(1, req.queryParams("email"));
                    pstmt.executeUpdate();
                    res.status(200);
                    return "User has been successfully deleted";
                } catch (SQLException e) {
                    e.printStackTrace();
                    return "An error occurred when deleting the user";
                }
            });

            // DELETE on /users to delete a user
            post("/login", (req, res) -> {
                try {
                    PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM Users WHERE email = ? AND password = ?");
                    pstmt.setString(1, req.queryParams("email"));
                    pstmt.setString(2, req.queryParams("password"));
                    ResultSet rs = pstmt.executeQuery();
                    if (rs.next()){
                        res.status(200);
                        return "User is logged in";
                    }
                    else{
                        res.status(404);
                        return "Invalid User or Password";
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                    res.status(400);
                    return "An error occurred when logging the user";
                }
            });

            // GET on /bids to fetch all bids
            get("/bids", (req, res) -> {
                try {
                    // Execute SQL query to retrieve bids
                    Statement stmt = conn.createStatement();
                    ResultSet rs = stmt.executeQuery("SELECT * FROM Bids");

                    JSONArray jsonArray = new JSONArray();

                    while (rs.next()) {
                        JSONObject bidJson = new JSONObject();
                        bidJson.put("id", rs.getInt("id"));
                        bidJson.put("purchase_price", rs.getFloat("purchase_price"));
                        bidJson.put("time", rs.getTimestamp("time"));
                        bidJson.put("quantity", rs.getInt("quantity"));
                        bidJson.put("email", rs.getString("email"));
                        bidJson.put("sale_id", rs.getInt("sale_id"));
                        jsonArray.put(bidJson);
                    }
                    res.type("application/json");
                    return jsonArray;

                } catch (SQLException e) {
                    e.printStackTrace();
                    return "An error occurred when retrieving bids";
                }
            });




            // GET on /bids/{id} to fetch a specific bid
            get("/bids/:id", (req, res) -> {
                try {
                    // Execute SQL query to retrieve bid
                    PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM Bids WHERE id = ?");
                    pstmt.setInt(1, Integer.parseInt(req.params(":id")));
                    ResultSet rs = pstmt.executeQuery();
                    JSONObject bidJson = new JSONObject();
                    if (rs.next()) {
                        bidJson.put("id", rs.getInt("id"));
                        bidJson.put("purchase_price", rs.getFloat("purchase_price"));
                        bidJson.put("time", rs.getTimestamp("time"));
                        bidJson.put("quantity", rs.getInt("quantity"));
                        bidJson.put("email", rs.getString("email"));
                        bidJson.put("sale_id", rs.getInt("sale_id"));
                        res.type("application/json");
                    }
                    return bidJson;

                } catch (SQLException e) {
                    e.printStackTrace();
                    return "An error occurred when retrieving this bid";
                }
            });




            // POST on /bids to create a new bid
            post("/bids", (req, res) -> {
                try {
                    PreparedStatement pstmt = conn.prepareStatement("INSERT INTO Bids (purchase_price, time, quantity, email, sale_id) VALUES (?, ?, ?, ?,?)");
                    pstmt.setFloat(1, Float.parseFloat(req.queryParams("purchase_price")));
                    pstmt.setTimestamp(2, new Timestamp(System.currentTimeMillis())); // Assuming current time
                    pstmt.setInt(3, 1);
                    pstmt.setString(4, req.queryParams("email"));
                    pstmt.setInt(5, Integer.parseInt(req.queryParams("sale_id")));
                    pstmt.executeUpdate();

                    // Return success message
                    res.status(201); // HTTP status code 201 indicates successful creation
                    return "Bid created successfully";
                } catch (SQLException e) {
                    e.printStackTrace();
                    return "An error occurred when creating the bid";
                }
            });




            // PUT on /bids/{id} to update a bid
            put("/bids/:id", (req, res) -> {
                try {
                    PreparedStatement pstmt = conn.prepareStatement("UPDATE Bids SET purchase_price = ?, time = ?, quantity = ?, email = ?, sale_id = ? WHERE id = ?");
                    pstmt.setFloat(1, Float.parseFloat(req.queryParams("purchase_price")));
                    pstmt.setTimestamp(2, new Timestamp(System.currentTimeMillis())); // Assuming current time
                    pstmt.setInt(3, Integer.parseInt(req.queryParams("quantity")));
                    pstmt.setString(4, req.queryParams("email"));
                    pstmt.setInt(5, Integer.parseInt(req.queryParams("sale_id")));
                    pstmt.setInt(6, Integer.parseInt(req.params(":id")));
                    pstmt.executeUpdate();

                    // Return success message
                    res.status(201); // HTTP status code 201 indicates successful creation
                    return "Bid information updated successfully";
                } catch (SQLException e) {
                    e.printStackTrace();
                    return "An error occurred when updating bid information";
                }
            });




            // DELETE on /bids/{id} to delete a bid
            delete("/bids/:id", (req, res) -> {
                try {
                    PreparedStatement pstmt = conn.prepareStatement("DELETE FROM Bids WHERE id = ?");
                    pstmt.setInt(1, Integer.parseInt(req.params(":id")));
                    pstmt.executeUpdate();
                    res.status(200);
                    return "Bid has been successfully deleted";
                } catch (SQLException e) {
                    e.printStackTrace();
                    return "An error occurred when deleting the bid";
                }
            });



            // GET on /sales to fetch all sales
            get("/sales", (req, res) -> {
                try {
                    // Execute SQL query to retrieve sales
                    Statement stmt = conn.createStatement();
                    ResultSet rs = stmt.executeQuery("SELECT * FROM Sales");

                    JSONArray jsonArray = new JSONArray();

                    while (rs.next()) {
                        JSONObject saleJson = new JSONObject();
                        saleJson.put("id", rs.getInt("id"));
                        saleJson.put("product_name", rs.getString("product_name"));
                        saleJson.put("price", rs.getFloat("price"));
                        saleJson.put("revocable", rs.getBoolean("revocable"));
                        saleJson.put("overbid", rs.getBoolean("overbid"));
                        saleJson.put("duration", rs.getTimestamp("duration"));
                        saleJson.put("active", rs.getBoolean("active"));
                        saleJson.put("type", rs.getString("type"));
                        saleJson.put("email", rs.getString("email"));
                        saleJson.put("last_bid_time", rs.getTimestamp("last_bid_time"));
                        jsonArray.put(saleJson);
                    }
                    res.type("application/json");
                    return jsonArray;

                } catch (SQLException e) {
                    e.printStackTrace();
                    return "An error occurred when retrieving sales";
                }
            });

            get("/sales/active", (req, res) -> {
                try {
                    // Execute SQL query to retrieve sales
                    Statement stmt = conn.createStatement();
                    ResultSet rs = stmt.executeQuery("SELECT * FROM Sales WHERE active = 1");

                    JSONArray jsonArray = new JSONArray();

                    while (rs.next()) {
                        JSONObject saleJson = new JSONObject();
                        saleJson.put("id", rs.getInt("id"));
                        saleJson.put("product_name", rs.getString("product_name"));
                        saleJson.put("price", rs.getFloat("price"));
                        saleJson.put("revocable", rs.getBoolean("revocable"));
                        saleJson.put("overbid", rs.getBoolean("overbid"));
                        saleJson.put("duration", rs.getTimestamp("duration"));
                        saleJson.put("type", rs.getString("type"));
                        saleJson.put("email", rs.getString("email"));
                        saleJson.put("last_bid_time", rs.getTimestamp("last_bid_time"));
                        jsonArray.put(saleJson);
                    }
                    res.type("application/json");
                    return jsonArray;

                } catch (SQLException e) {
                    e.printStackTrace();
                    return "An error occurred when retrieving sales";
                }
            });

            // GET on /sales/:id to fetch a specific sale
            get("/sales/:id", (req, res) -> {
                try {
                    // Execute SQL query to retrieve sale
                    PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM Sales WHERE id = ?");
                    pstmt.setInt(1, Integer.parseInt(req.params(":id")));
                    ResultSet rs = pstmt.executeQuery();
                    JSONObject saleJson = new JSONObject();
                    if (rs.next()) {
                        saleJson.put("id", rs.getInt("id"));
                        saleJson.put("product_name", rs.getString("product_name"));
                        saleJson.put("price", rs.getFloat("price"));
                        saleJson.put("revocable", rs.getBoolean("revocable"));
                        saleJson.put("overbid", rs.getBoolean("overbid"));
                        saleJson.put("active", rs.getBoolean("active"));
                        saleJson.put("duration", rs.getTimestamp("duration"));
                        saleJson.put("type", rs.getString("type"));
                        saleJson.put("email", rs.getString("email"));
                        saleJson.put("last_bid_time", rs.getTimestamp("last_bid_time"));
                        res.type("application/json");
                    }
                    return saleJson;

                } catch (SQLException e) {
                    e.printStackTrace();
                    return "An error occurred when retrieving this sale";
                }
            });



            // POST on /sales to create a new sale
            post("/sales/illimited", (req, res) -> {
                try {
                    PreparedStatement pstmt = conn.prepareStatement("INSERT INTO Sales (product_name, price, revocable, overbid, type, email) VALUES (?, ?, ?, ?, ?, ?)");
                    pstmt.setString(1, req.queryParams("product_name"));
                    pstmt.setFloat(2, Float.parseFloat(req.queryParams("price")));
                    pstmt.setBoolean(3, Boolean.parseBoolean(req.queryParams("revocable")));
                    pstmt.setBoolean(4, Boolean.parseBoolean(req.queryParams("overbid")));
                    pstmt.setString(5, req.queryParams("type"));
                    pstmt.setString(6, req.queryParams("email"));
                    pstmt.executeUpdate();

                    // Return success message
                    res.status(200); // HTTP status code 201 indicates successful creation
                    return "Sale created successfully";
                } catch (SQLException e) {
                    e.printStackTrace();
                    res.status(201); // HTTP status code 201 indicates successful creation
                    return "An error occurred when creating the sale";
                }
            });

            post("/sales/limited", (req, res) -> {
                try {
                    PreparedStatement pstmt = conn.prepareStatement("INSERT INTO Sales (product_name, price, revocable, overbid, duration, type, email) VALUES (?, ?, ?, ?, ?, ?, ?)");
                    pstmt.setString(1, req.queryParams("product_name"));
                    pstmt.setFloat(2, Float.parseFloat(req.queryParams("price")));
                    pstmt.setBoolean(3, Boolean.parseBoolean(req.queryParams("revocable")));
                    pstmt.setBoolean(4, Boolean.parseBoolean(req.queryParams("overbid")));
                    pstmt.setTimestamp(5, Timestamp.valueOf(req.queryParams("duration")));
                    pstmt.setString(6, req.queryParams("type"));
                    pstmt.setString(7, req.queryParams("email"));
                    pstmt.executeUpdate();

                    // Return success message
                    res.status(200); // HTTP status code 201 indicates successful creation
                    return "Sale created successfully";
                } catch (SQLException e) {
                    e.printStackTrace();
                    res.status(201); // HTTP status code 201 indicates successful creation
                    return "An error occurred when creating the sale";
                }
            });

            put("/sales/:id", (req, res) -> {
                try {
                    PreparedStatement pstmt = conn.prepareStatement("UPDATE Sales SET price = ?, last_bid_time = ? WHERE id = ?");
                    pstmt.setFloat(1, Float.parseFloat(req.queryParams("price")));
                    pstmt.setTimestamp(2, new Timestamp(System.currentTimeMillis()));
                    pstmt.setInt(3, Integer.parseInt(req.params(":id")));
                    pstmt.executeUpdate();

                    res.status(201);
                    return "Sale information updated successfully";
                } catch (SQLException e) {
                    e.printStackTrace();
                    return "An error occurred when updating sale information";
                }
            });

            put("/sales/:id/archived", (req, res) -> {
                try {
                    PreparedStatement pstmt = conn.prepareStatement("UPDATE Sales SET active = 0 WHERE id = ?");
                    pstmt.setInt(1, Integer.parseInt(req.params(":id")));
                    pstmt.executeUpdate();

                    res.status(201);
                    return "Sale information updated successfully";
                } catch (SQLException e) {
                    e.printStackTrace();
                    return "An error occurred when updating sale information";
                }
            });


            // DELETE on /sales/:id to delete a sale
            delete("/sales/:id", (req, res) -> {
                try {
                    PreparedStatement pstmt = conn.prepareStatement("DELETE FROM Sales WHERE id = ?");
                    pstmt.setInt(1, Integer.parseInt(req.params(":id")));
                    int rowsAffected = pstmt.executeUpdate();

                    res.status(200);
                    return "Sale has been deleted successfully";

                } catch (SQLException e) {
                    e.printStackTrace();
                    res.status(500);
                    return "An error occurred when deleting the sale";
                }
            });

            // GET on /sales/:id/bids to fetch bids of a specific sale
            get("/sales/:id/bids", (req, res) -> {
                try {
                    // Execute SQL query to retrieve sale
                    PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM Bids WHERE sale_id = ? ORDER BY time DESC");
                    pstmt.setInt(1, Integer.parseInt(req.params(":id")));
                    ResultSet rs = pstmt.executeQuery();
                    List<JSONObject> bidsJson = new ArrayList();
                    while (rs.next()) {
                        JSONObject bidJson = new JSONObject();
                        bidJson.put("id", rs.getInt("id"));
                        bidJson.put("purchase_price", rs.getFloat("purchase_price"));
                        bidJson.put("time", rs.getTimestamp("time"));
                        bidJson.put("bids.email", rs.getString("email"));
                        bidsJson.add(bidJson);
                    }
                    res.type("application/json");
                    return bidsJson;

                } catch (SQLException e) {
                    e.printStackTrace();
                    return "An error occurred when retrieving bids for a specific sale";
                }
            });
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}